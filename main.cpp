#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    const double eps = 0.0001;
    double x = 0, an, sum;
    int n;

    cout.setf(ios::fixed);

    cout << "y(x) = (exp(x)-exp(-x))/2\t\tS(n) = sum[n=0..k] x^(2n+1)/(2n+1)!\n\n";
    for(int i = 0; i < 10; i++)
    {
        x += 0.1;
        sum = an = x;
        for(int i = 1; i < 20; i++)
        {
            an *= pow(x, 2) / (2 * i + 2) / (2 * i + 3);
            sum += an;
        }
        cout << "x = " << x << "\tS(n) = " << sum;

        n = 1;
        sum = an = x;
        do
        {
            an *= pow(x, 2) / (2 * n + 2) / (2 * n + 3);
            sum += an;
            n++;
        } while(fabs(an) > eps);
        cout << " \tS(eps) = " << sum << " (n = " << n << ")\ty(x) = " << (exp(x)-exp(-x)) / 2 << "\n";

    }

    return 0;
}
